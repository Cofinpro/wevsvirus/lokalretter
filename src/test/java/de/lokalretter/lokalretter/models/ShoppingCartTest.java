package de.lokalretter.lokalretter.models;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class ShoppingCartTest {

    @Test
    void addToCardOnEmpty() {
        Article eis = new Article("Vanilleeis", BigDecimal.ONE);
        ShoppingCart shoppingCart = new ShoppingCart();
        Integer quantity = shoppingCart.addToCard(eis);
        assertEquals(1, quantity);
        quantity = shoppingCart.addToCard(eis);
        assertEquals(2, quantity);
        quantity = shoppingCart.getArticles().get(eis);
        assertEquals(2, quantity);
    }
}