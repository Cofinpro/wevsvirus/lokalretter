package de.lokalretter.lokalretter.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "SHOPS")
public class ShopEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SHOP_ID_SEQ")
    @SequenceGenerator(name = "SHOP_ID_SEQ", sequenceName = "SHOP_ID_SEQ", allocationSize = 1)
    private Long id;
    private String emailAddress;
    private String password;
    private String companyName;
    private String name;
    private String surname;
    private String ustId;
    private String street;
    private String streetNumber;
    private String zipCode;
    private String city;
    private String website;
    private String aboutThisShop;

}
