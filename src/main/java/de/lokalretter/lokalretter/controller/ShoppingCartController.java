package de.lokalretter.lokalretter.controller;

import de.lokalretter.lokalretter.models.Article;
import de.lokalretter.lokalretter.models.ShoppingCart;
import de.lokalretter.lokalretter.service.ArticleService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.SessionScope;

@SessionScope
@Controller
public class ShoppingCartController {

    private ShoppingCart shoppingCart = new ShoppingCart();
    private ArticleService articleService;

    @Autowired
    public ShoppingCartController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping("/cart")
    public String getCart(Model model) {
        model.addAttribute("items", shoppingCart.getArticles());
        model.addAttribute("sum", shoppingCart.getTotal());
        return "cart";
    }

    @GetMapping("/sortiment")
    public String getSortiment(Model model) {
        model.addAttribute("articles", this.articleService.getArticles());
        return "sortiment";
    }

    @PostMapping("/cart/add")
    public String addArticle(Article article) {
        shoppingCart.addToCard(article);
        return "redirect:/sortiment";
    }

}
