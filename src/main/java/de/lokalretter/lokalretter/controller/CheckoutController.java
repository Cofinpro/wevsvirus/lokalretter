package de.lokalretter.lokalretter.controller;

import com.stripe.exception.StripeException;
import de.lokalretter.lokalretter.service.StripeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CheckoutController {

    private StripeService stripeService;

    @Autowired
    public CheckoutController(StripeService stripeService) {
        this.stripeService = stripeService;
    }

    @GetMapping("/delivery")
    public String delivery(Model model) throws StripeException {
        model.addAttribute("stripeSession", stripeService.startCheckout());
        return "delivery";
    }

    @GetMapping("/success")
    public String success() {
        return "success";
    }
}
