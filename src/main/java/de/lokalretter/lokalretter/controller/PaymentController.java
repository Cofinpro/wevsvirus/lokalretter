package de.lokalretter.lokalretter.controller;

import com.stripe.exception.StripeException;
import de.lokalretter.lokalretter.service.StripeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PaymentController {

    private final StripeService stripeService;
    private final Logger log = LoggerFactory.getLogger(getClass());

    public PaymentController(StripeService stripeService) {
        this.stripeService = stripeService;
    }

    @GetMapping("checkout")
    public String checkout(Model model) {
        try {
            model.addAttribute("clientSecret", stripeService.createPaymentIntent());
        } catch (StripeException e) {
            log.error("Could not request payment intent", e);
        }
        return "payment";

    }
}
