package de.lokalretter.lokalretter.controller;

import de.lokalretter.lokalretter.Exceptions.NotFoundException;
import de.lokalretter.lokalretter.entities.ShopEntity;
import de.lokalretter.lokalretter.mapper.ShopMapper;
import de.lokalretter.lokalretter.models.Shop;
import de.lokalretter.lokalretter.repository.ShopRepository;
import de.lokalretter.lokalretter.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ShopController {

    private ShopRepository shopRepository;
    private ShopMapper shopMapper;
    private ArticleService articleService;


    @Autowired
    public ShopController(ShopRepository shopRepository, ShopMapper shopMapper, ArticleService articleService) {
        this.shopRepository = shopRepository;
        this.shopMapper = shopMapper;
        this.articleService = articleService;
    }

    @GetMapping("/shop")
    public String shop() {
        return "shop";
    }

    @GetMapping("/registershop")
    public String registerShop(Model model) {
        Shop newShop = new Shop();
        model.addAttribute("newShop", newShop);
        return "registershop";
    }

    @PostMapping("/createshop")
    public String submitShop(@ModelAttribute Shop shop) {
        ShopEntity shopEntity = shopMapper.shopToShopEntity(shop);
        ShopEntity save = shopRepository.save(shopEntity);
        return "redirect:/shop/" + save.getId();
    }

    @GetMapping("/shops")
    @ResponseBody
    public List<Shop> getAllShops() {
        List<ShopEntity> shopEntities = shopRepository.findAll();
        return shopMapper.shopEntitiesToShops(shopEntities);
    }

    @GetMapping("/shop/{id}")
    public String shopDetail(Model model, @PathVariable("id")Long id) {
        ShopEntity shopEntity = shopRepository.findById(id).orElseThrow(NotFoundException::new);
        Shop shop = shopMapper.shopEntityToShop(shopEntity);
        model.addAttribute("shop", shop);
        return "shopdetails";
    }

    @GetMapping("/dealer-articles")
    public String getShopSortiment(Model model) {
        model.addAttribute("articles", this.articleService.getArticles());
        return "dealer-articles";
    }

}
