package de.lokalretter.lokalretter.controller;

import de.lokalretter.lokalretter.models.Shop;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StaticSiteController {

    @GetMapping("/")
    public String getIndex() {
        return "index";
    }

    @GetMapping("/faq")
    public String getFaq() {
        return "faq";
    }

    @GetMapping("/about")
    public String getAbout() {
        return "about";
    }

}
