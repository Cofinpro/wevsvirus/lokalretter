package de.lokalretter.lokalretter.controller;

import de.lokalretter.lokalretter.Exceptions.NotFoundException;
import de.lokalretter.lokalretter.entities.ShopEntity;
import de.lokalretter.lokalretter.mapper.ShopMapper;
import de.lokalretter.lokalretter.models.Shop;
import de.lokalretter.lokalretter.repository.ShopRepository;
import de.lokalretter.lokalretter.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dealer")
public class DealerController {

    @Autowired
    private ShopRepository shopRepository;

    @Autowired
    private ShopMapper shopMapper;

    @Autowired
    private ArticleService articleService;

    @GetMapping()
    public String getDealerOverview() {
        return "redirect:/dealer/orders";
    }

    @GetMapping("/orders")
    public String getDealerOrders() {
        return "dealer-orders";
    }

    @GetMapping("/pickup")
    public String getDealerPickup() {
        return "dealer-pickup";
    }

    @GetMapping("/delivery")
    public String getDealerDelivery() {
        return "dealer-delivery";
    }

    @GetMapping("/payment")
    public String getDealerPayment() {
        return "dealer-payment";
    }

    @GetMapping("/articles")
    public String getDealerArticles(Model model) {
        model.addAttribute("articles", this.articleService.getArticles());
        return "dealer-articles";
    }

    @GetMapping("/profile")
    public String getDealerProfile(Model model) {
        ShopEntity shopEntity = shopRepository.findById(1L).orElseThrow(NotFoundException::new);
        Shop shop = shopMapper.shopEntityToShop(shopEntity);
        model.addAttribute("shop", shop);
        return "dealer-profile";
    }

    @PostMapping("/profile")
    public String saveShop(@ModelAttribute Shop shop) {
        ShopEntity shopEntity = shopMapper.shopToShopEntity(shop);
        shopEntity.setId(1L);
        shopRepository.save(shopEntity);
        return "redirect:/dealer/profile";
    }
}
