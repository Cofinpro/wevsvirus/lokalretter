package de.lokalretter.lokalretter.models;

import org.springframework.web.context.annotation.SessionScope;

import java.math.BigDecimal;
import java.util.*;

@SessionScope
public class ShoppingCart {

    private Map<Article, Integer> articles = new HashMap<>();

    public Integer addToCard(Article article) {
        return articles.compute(article, (art, quantity) -> Optional.ofNullable(quantity).orElse(0) + 1);
    }

    public Map<Article, Integer> getArticles() {
        return Collections.unmodifiableMap(articles);
    }

    public BigDecimal getTotal() {
        Iterator it = articles.entrySet().iterator();
        BigDecimal total = BigDecimal.ZERO;

        while (it.hasNext()) {
            Map.Entry<Article, Integer> pair = (Map.Entry) it.next();
            Article article = pair.getKey();
            BigDecimal quantity = BigDecimal.valueOf(pair.getValue());

            total = total.add(article.getPrice().multiply(quantity));
        }

        return total;
    }
}
