package de.lokalretter.lokalretter.service;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class StripeService {

    public StripeService(@Value("${stripe.apikey}") String stripeApiKey) {
        Stripe.apiKey = stripeApiKey;
    }

    public String createPaymentIntent() throws StripeException {
        final PaymentIntent paymentIntent = PaymentIntent.create(
                Map.of("amount", 2000,
                        "currency", "eur",
                        "payment_method_types", List.of("card")));
        return paymentIntent.getClientSecret();
    }

    public String startCheckout() throws StripeException {
        Stripe.apiKey = "sk_test_xiR2gfLTQKprzqcUT58fXNpp00IGERHYa0";

        SessionCreateParams params =
                SessionCreateParams.builder()
                        .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)

                        .addLineItem(
                                SessionCreateParams.LineItem.builder()
                                        .setName("Weizenbrötchen")
                                        .setAmount(150L)
                                        .setCurrency("eur")
                                        .setQuantity(3L)
                                        .build())
                        .addLineItem(
                                SessionCreateParams.LineItem.builder()
                                        .setName("Roggenbrötchen")
                                        .setAmount(50L)
                                        .setCurrency("eur")
                                        .setQuantity(1L)
                                        .build())
                        .setSuccessUrl("http://www.lokalretter.de/success?session_id={CHECKOUT_SESSION_ID}")
                        .setCancelUrl("http://www.lokalretter.de/cancel")
                        .build();

        Session session = Session.create(params);
        return session.getId();
    }
}
