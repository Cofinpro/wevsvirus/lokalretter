package de.lokalretter.lokalretter.service;

import de.lokalretter.lokalretter.models.Article;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

@Service
public class ArticleService {


    private Collection<Article> articles = List.of(new Article("Weizenbrötchen", new BigDecimal("0.50")),
            new Article("Roggenbrötchen", new BigDecimal("0.80")),
            new Article("Franzbrötchen", new BigDecimal("1.00")),
            new Article("Kartoffelbrötchen", new BigDecimal("0.85"))
    );

    public Collection<Article> getArticles() {

        return  articles;
    }

    public void addArticle(Article article) {
        articles.add(article);
    }
}
