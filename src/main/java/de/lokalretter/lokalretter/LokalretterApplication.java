package de.lokalretter.lokalretter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LokalretterApplication {

	public static void main(String[] args) {
		SpringApplication.run(LokalretterApplication.class, args);
	}



}
