package de.lokalretter.lokalretter.mapper;

import de.lokalretter.lokalretter.entities.ShopEntity;
import de.lokalretter.lokalretter.models.Shop;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ShopMapper {

    ShopEntity shopToShopEntity(Shop shop);

    List<Shop> shopEntitiesToShops(List<ShopEntity> shopEntities);

    Shop shopEntityToShop(ShopEntity shopEntity);
}
