package de.lokalretter.lokalretter.repository;

import de.lokalretter.lokalretter.entities.ShopEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShopRepository extends CrudRepository<ShopEntity, Long> {

    List<ShopEntity> findAll();
}
