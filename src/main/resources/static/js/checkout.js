var stripe = Stripe("pk_test_A8bLIPqomSrFAd0XnT6mO18200HTseMFHD");

function checkout(e) {
    e.preventDefault();
    const stripeSession = document.getElementById('stripeSession').value;
    stripe.redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId: stripeSession
    }).then(function (result) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
    });
}