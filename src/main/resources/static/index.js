var map;
var geoCoder;
var infoWindow;
var placesService;
var autocomplete;

var markers;

window.onload = function () {
    var input = document.getElementById('plz');

    var options = {
        componentRestrictions: {country: ['de']},
        types: ['(regions)']
    };
    autocomplete = new google.maps.places.Autocomplete(input, options);

    autocomplete.addListener('place_changed', function () {
        map.setCenter(autocomplete.getPlace().geometry.location);
        map.setZoom(15);
        new google.maps.Marker({
            map: map,
            position: autocomplete.getPlace().geometry.location
        });
        findMarkers();
    })
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 51.1657, lng: 10.4515},
        zoom: 6
    });

    geoCoder = new google.maps.Geocoder();

    infoWindow = new google.maps.InfoWindow;
    placesService = new google.maps.places.PlacesService(map);

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            infoWindow.open(map);
            map.setCenter(pos);
            map.setZoom(12);
            findMarkers();
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

    map.addListener('bounds_changed', function () {
        findMarkers();
    });
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

function plzChanged() {
    let zip = document.getElementById('plz').value;

    if (zip.length !== 5) return;

    geoCoder.geocode({'address': zip}, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            //Got result, center the map and put it out there
            map.setCenter(results[0].geometry.location);
            map.setZoom(15);
            new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
            findMarkers();
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
}

function shopChanged() {
    let shopName = document.getElementById('shopName').value;

    var request = {
        query: shopName,
        fields: ['name', 'geometry']
    };


    placesService.findPlaceFromQuery(request, function (results) {
        map.setCenter(results[0].geometry.location);
        map.setZoom(15);
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location
        });
        infoWindow.setContent(createInfoWindowContent(results[0]));
        infoWindow.open(map, marker);
    });

}

function findMarkers() {
    var request = {
        type: 'bakery',
        location: map.getCenter()
    };

    placesService.textSearch(request, function (results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            markers = results.map(place => {
                return createMarker(place);
            });
            const clusterer = new MarkerClusterer(map, markers,
                {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
            console.dir(clusterer);
        }
    });
}

function createMarker(place) {
    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
    });

    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.setContent(createInfoWindowContent(place));
        infoWindow.open(map, this);
    });
    return marker;
}

function createInfoWindowContent(place) {
    let infoContent = document.getElementById('infoWindowTemplate');

    let title = infoContent.content.children['title'];
    let link = infoContent.content.children['link'];

    title.innerText = place.name;
    link.href = '/sortiment';

    return infoContent.innerHTML;

}